<?php
include './includes/classAutoloader.inc.php';
?>
<!DOCTYPE html>
<html lang="en">
<?php 
include './assets/Templates/template-header.html';
?>
<body>
    <div class="container pt-4 mt-4">
        <div class="header-group row pr-4">
            <h1>Product List</h1>
            <div class="button-group">
                <button class="btn btn-primary" onclick="window.location='productAddView.php';return false;">Add</button>
                <button class="btn btn-danger" form="deleteProducts" type="submit" name="delete">Mass Delete</button>
            </div>
        </div>
        <hr>

        <form method="POST" id="deleteProducts" action="remove.php">
            <div class="row pl-5">
                <?php
                $prodobj = new ProductView();
                $products = $prodobj->getAllProducts();
                if (is_array($products)) {
                    foreach ($products as $product) {
                        echo "<div class=\"col-md-3 mr-5 mt-3 border\">
                            <input type=\"checkbox\" name=\"check-deleted[]\" value=\"{$product->SKU}\" />
                            <div class=\"inner\">
                                <p><b>SKU</b>: " . $product->SKU . "</p>
                                <p><b>Name</b>: " . $product->Name . "</p>
                                <p><b>Price($)</b>: " . $product->Price . "</p>
                                <p><b>" . $product->Type . "</b>: " . $product->Attribute . "</p>
                            </div>
                        </div>";
                    }
                }
                ?>
            </div>
        </form>
    </div>
</body>

</html>