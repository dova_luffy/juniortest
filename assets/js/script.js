$(document).ready(function() {

    $('#type').change(function() {
        var id = $(this).children(":selected").attr("id");
        var templateType = id;
        var template = `./assets/Templates/template-${templateType}.html`;
        $("#template").load(template);
    });
});