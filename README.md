# Junior Developer Test Task #
  
A web-app containing two pages for

1. Product list page
2. Adding a product page

This is the README file for the Junior Developer Test Task

### Languages used ###

* HTML
* Vanilla PHP
* Jquery
* Bootstrap
* MySQL

### Important Links ###

* [Project Repo](https://bitbucket.org/dova_luffy/juniortest/src/main/)
* [Project Website](https://test-lotfi.000webhostapp.com/)
