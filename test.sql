-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2021 at 03:47 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `ID` int(3) NOT NULL,
  `Category_name` varchar(60) NOT NULL,
  `Type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--
-- Important data for the website to work properly
INSERT INTO `category` (`ID`, `Category_name`, `Type`) VALUES
(1, 'Book', 'Weight'),
(2, 'DVD', 'Size'),
(3, 'Furniture ', 'Dimension');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `SKU` varchar(60) NOT NULL,
  `Name` varchar(60) NOT NULL,
  `Price` decimal(6,2) NOT NULL,
  `CatId` int(3) NOT NULL,
  `Attribute` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`SKU`, `Name`, `Price`, `CatId`, `Attribute`) VALUES
('DSJ321', 'Furiniture', '50.00', 3, '100cm x 100cm x 100cm'),
('FDH786', 'Sofa', '700.00', 3, '72cm x 220cm x 86cm'),
('HRE345', 'Ikea table', '50.00', 3, '50cm x 50cm x 50cm'),
('IOU897', 'Book', '5.00', 1, '1 kg'),
('JET324', 'Ikea table', '20.00', 3, '50cm x 50cm x 50cm'),
('KHD457', 'DISK', '10.00', 2, '1200 Mb'),
('KJH897', 'Book', '20.00', 1, '2 kg'),
('LJD094', 'DVD', '10.00', 2, '500 Mb');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`SKU`),
  ADD KEY `CatId` (`CatId`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `CatId` FOREIGN KEY (`CatId`) REFERENCES `category` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
