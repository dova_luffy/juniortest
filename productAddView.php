<?php
include './includes/classAutoloader.inc.php';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (isset($_POST['submit'])) {

        $product = new $_POST['Type'];
        $product->setSKU($_POST['SKU']);
        $product->setName($_POST['Name']);
        $product->setPrice($_POST['Price']);
        $product->setAttribute($_POST['Attribute']);
        $product->saveProduct();

        header("Location: index.php");
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<?php 
include './assets/Templates/template-header.html';
?>

<body>
    <div class="container pt-4 mt-4">
        <form method="POST">
            <div class="header-group row pr-4">
                <h1>Product Add</h1>
                <div class="button-group">

                    <button type="submit" class="btn btn-primary" name="submit" value="submit">Save</button>

                    <button class="btn btn-danger" type="cancel" onclick="window.location='index.php';return false;">Cancel</button>

                </div>
            </div>
            <hr>
            <div class="form-group row">
                <label for="inputSKU" class="col-sm-2 col-form-label">SKU</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="inputSKU" name="SKU" placeholder="SKU Code (6 digits)" minlength="6" maxlength="6" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="inputName" name="Name" placeholder="Product Name" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPrice" class="col-sm-2 col-form-label">Price</label>
                <div class="col-sm-3">
                    <input type="number" step="0.01" class="form-control" id="inputPrice" name="Price" placeholder="Product Price ($)" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="type" class="col-sm-2 col-form-label">Type Switcher</label>
                <div class="col-sm-3">
                    <select class="custom-select" id="type" name="Type" required>
                        <option value="" id="empty">Choose Switcher Type</option>
                        <option value="book" id="book">Book</option>
                        <option value="dvd" id="dvd">DVD</option>
                        <option value="furniture" id="furniture">Furniture</option>
                    </select>
                </div>
            </div>
            <div id="template"></div>
        </form>
    </div>
</body>



</html>