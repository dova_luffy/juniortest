<?php

class ProductView extends Database
{
    public function getAllProducts()
    {
        $sql = "SELECT * FROM products INNER JOIN category ON products.CatId = category.ID";

        if ($result = $this->connect()->query($sql)) {
            $data = $result->fetchAll(PDO::FETCH_OBJ);
        }
        return $data;
    }

    // deleteProducts funtion deletes one or more data from the database
    public function deleteProducts($list)
    {
        $ProductsToRemove = "('" . implode("', '", $list) . "')";
        $sql = "DELETE FROM Products WHERE SKU IN $ProductsToRemove";
        $this->connect()->query($sql);
    }
}
