<?php
class Database{
   
    private $host = "localhost";
    private $db_name = "test";
    private $username = "root";
    private $password = "";

    public function connect(){
        $dsn = 'mysql:host=' . $this->host .';dbname='.$this->db_name;
        $pdo = new PDO($dsn, $this->username, $this->password);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    }
}
?>