<?php
abstract class Products extends Database
{
    protected $SKU;
    protected $Name;
    protected $Price;

    public function getSKU()
    {
        return $this->SKU;
    }

    public function getName()
    {
        return $this->Name;
    }

    public function getPrice()
    {
        return $this->Price;
    }

    function setSKU($SKU)
    {
        $this->SKU = $SKU;
    }

    function setName($name)
    {
        $this->Name = $name;
    }

    function setPrice($price)
    {
        $this->Price = $price;
    }

    public function saveProduct()
    {
        $sql = "INSERT INTO products (SKU, Name, Price, CatId, Attribute) VALUES (?, ?, ?, ?, ?)";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array(trim(htmlspecialchars($this->SKU)), trim(htmlspecialchars($this->Name)), filter_var($this->Price, FILTER_SANITIZE_NUMBER_INT), $this->getID() , trim(htmlspecialchars($this->getAttribute()))));
        $stmt = null;
    }
    
    abstract public function getID();
    abstract public function getAttribute();
    abstract public function setAttribute($attribute);
}
