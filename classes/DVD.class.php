<?php
class Dvd extends Products
{

    protected $Size;
    protected const ID = 2;


    public function getID()
    {
        return self::ID;
    }
    public function getAttribute()
    {
        return $this->Size;
    }

    function setAttribute($attribute)
    {
        $this->Size = $attribute['Size'] . " Mb";
    }
}
