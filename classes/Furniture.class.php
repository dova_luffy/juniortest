<?php

class Furniture extends Products
{

    protected $height;
    protected $width;
    protected $length;
    protected const ID = 3;


    public function getID()
    {
        return self::ID;
    }

    public function getAttribute()
    {
        return $this->height . $this->width . $this->length;
    }
    public function setAttribute($attribute)
    {
        $this->height = $attribute['Height'] . "cm x ";
        $this->width = $attribute['Width'] . "cm x ";
        $this->length = $attribute['Length'] . "cm ";
    }
}
