<?php

class Book extends Products
{

    protected $weight;
    protected const ID = 1;


    public function getID()
    {
        return self::ID;
    }
    public function getAttribute()
    {
        return $this->weight;
    }
    public function setAttribute($attribute)
    {
        $this->weight = $attribute['Weight'] . " kg";
    }
}
